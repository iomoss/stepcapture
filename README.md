# StepCapture

This project connects an Arduino Mega 2560, to a 3D printer firmware, in order to sniff out the step signals.

## Connections
```
GND <=> GND

21 <=> XSTEP
18 <=> XDIR

20 <=> YSTEP
17 <=> YDIR

19 <=> ZSTEP
16 <=> ZDIR
```
