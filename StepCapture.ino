#include "BufferedSerial.h"

BufferedSerial serial = BufferedSerial(0, 4096);

// Setup everything
void setup()
{
    // Attach interrupt handlers
    attachInterrupt(4, on_change_z, RISING);
    attachInterrupt(3, on_change_y, RISING);
    attachInterrupt(2, on_change_x, RISING);
    
    // initialize the serial communication:
    serial.init(0, 115200);
    //serial.setPacketHandler(handlePacket);
}

// Ignore incoming data
// Wvoid handlePacket(ByteBuffer* packet){ }

byte bitfield = 0;

void on_change_z()
{
  on_change(3);
}

void on_change_y()
{
  on_change(4);
}

void on_change_x()
{
  on_change(5);
}

// Interrupt service routine for changes
void on_change(int step_pin)
{
    // Read the DIR pins
    for(int x=0; x<3; x++)
    {
      bitfield &= ~(1 << x);
      int value = digitalRead(16+x);
      bitfield |= value << x;
    }
    // Toggle the step pin
    bitfield ^= 1 << step_pin;
    // Write out the readings
    serial.sendSerialByte(bitfield);
    // Toggle the step pin
    bitfield ^= 1 << step_pin;
    // Write out the readings
    serial.sendSerialByte(bitfield);
}

// Write whatever needs to be written
void loop()
{
  serial.update();
}
